﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SyncPizza.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "hello";
            return View();
        }

        public ActionResult Chooser()
        {
            var pizzas = new List<Models.Pizza>();
            pizzas.Add(new Models.Pizza() { Name="Chicken Feast", Id=1, ImageName= "chicken_feast.png" });
            pizzas.Add(new Models.Pizza() { Name = "Ham & Pineapple", Id = 2, ImageName= "ham_and_pineapple.png" });
            pizzas.Add(new Models.Pizza() { Name = "Meateor", Id = 3, ImageName= "meateor.png" });
            pizzas.Add(new Models.Pizza() { Name = "Meatilicious", Id = 4, ImageName = "meatilicious.png" });
            ViewBag.Pizzas = pizzas;
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}