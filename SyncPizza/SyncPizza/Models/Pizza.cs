﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SyncPizza.Models
{
    public class Pizza
    {
        public string Name
        {
            get;
            set;
        }
        public int Id
        {
            get;
            set;
        }
        public string ImageName
        {
            get;
            set;
        }
    }
}